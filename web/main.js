if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'main'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'main'.");
}
var main = function (_, Kotlin) {
  'use strict';
  var throwCCE = Kotlin.throwCCE;
  var Unit = Kotlin.kotlin.Unit;
  var ensureNotNull = Kotlin.ensureNotNull;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  function button$lambda(closure$onclick) {
    return function (it) {
      closure$onclick();
      return Unit;
    };
  }
  function button(text, onclick) {
    var tmp$;
    var button = Kotlin.isType(tmp$ = document.createElement('button'), HTMLButtonElement) ? tmp$ : throwCCE();
    button.textContent = text;
    button.onclick = button$lambda(onclick);
    return button;
  }
  function main() {
    ensureNotNull(document.querySelector('#output')).append((new Counter()).body);
  }
  function Counter() {
    this.value = 0;
    this.body = document.createElement('div');
    this.out = document.createElement('div');
    this.up = button('Increment', Counter$up$lambda(this));
    this.down = button('Decrement', Counter$down$lambda(this));
    this.body.append(this.out, this.up, this.down);
    this.update();
  }
  Counter.prototype.update = function () {
    this.out.textContent = 'The current count is ' + this.value;
  };
  function Counter$up$lambda(this$Counter) {
    return function () {
      var tmp$;
      tmp$ = this$Counter.value;
      this$Counter.value = tmp$ + 1 | 0;
      this$Counter.update();
      return Unit;
    };
  }
  function Counter$down$lambda(this$Counter) {
    return function () {
      var tmp$;
      tmp$ = this$Counter.value;
      this$Counter.value = tmp$ - 1 | 0;
      this$Counter.update();
      return Unit;
    };
  }
  Counter.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Counter',
    interfaces: []
  };
  _.button_a4mwiz$ = button;
  _.main = main;
  _.Counter = Counter;
  main();
  Kotlin.defineModule('main', _);
  return _;
}(typeof main === 'undefined' ? {} : main, kotlin);
