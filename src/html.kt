import org.w3c.dom.HTMLButtonElement
import kotlin.browser.document

fun button(text: String, onclick: () -> Unit): HTMLButtonElement {
    val button = document.createElement("button") as HTMLButtonElement
    button.textContent = text
    button.onclick = { onclick() }
    return button
}
