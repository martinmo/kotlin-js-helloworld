import kotlin.browser.document

fun main() = document.querySelector("#output")!!.append(Counter().body)

class Counter {

    var value: Int = 0
    val body = document.createElement("div")
    val out = document.createElement("div")
    val up = button("Increment") {
        value++
        update()
    }
    val down = button("Decrement") {
        value--
        update()
    }

    init {
        body.append(out, up, down)
        update()
    }

    fun update() {
        out.textContent = "The current count is $value"
    }
}
